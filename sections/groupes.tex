\setchapterstyle{kao}
\setchapterpreamble[u]{\margintoc}
\chapter{Groupes, Anneaux, Corps}
\labch{groupes}

\section{Groupes}
\labsec{groupes}

\begin{definition}[Groupes]
	\labdef{groupes}
	Un groupe est un ensemble $G$ muni d'une opération binaire
	\begin{align*}
		\boldsymbol{\cdot}: G \times G &\to G\\
		a,b &\mapsto a\boldsymbol{\cdot} b
	\end{align*}
	Noté $(G, \boldsymbol{\cdot}\ )$, satisfaisant les axiomes suivants:
	\begin{enumerate}[label=(\arabic*)]
		\item L'associativité de l'opération
			$$\forall a,b,c \in G,\ (a\cdot b)\cdot c = a\cdot (b\cdot c)$$
		\item L'existence d'un élément neutre, noté $e$
			$$\exists e \in G,\ \forall g \in G,\ e\cdot g = g\cdot e = g$$
		\item L'existence d'un inverse
			$$\forall g \in G,\ \exists g^{-1} \in G,\ g\cdot g^{-1} = g^{-1}\cdot g = e$$
	\end{enumerate}
\end{definition}

\begin{definition}[Groupes abéliens]
	\labdef{abéliens}
	Un groupe abélien est un \hyperref[def:groupes]{groupe} satisfaisant également l'axiome (4) de la commutativité
	$$\forall a,b \in G,\ a\cdot b = b\cdot a$$
\end{definition}

\begin{example} Quelques exemples de groupes
	\labexample{groupes}
	\begin{itemize}
		\item $(\Z, +)$ est un groupe abélien (les $4$ axiomes sont vrais)
		\item $(\N, +)$ n'est pas un groupe (tout les nombres ne possèdent pas d'inverse)
		\item $(\Z, \cdot\ )$ n'est pas un groupe (tous les nombres ne possèdent pas d'inverse)
		\item $(\Q, +)$ est un groupe abélien
		\item $(\Q^*, \cdot\ )$ est un groupe abélien
		\item $(\Q, \cdot\ )$ n'est pas un groupe (pas d'inverse pour $0$)
		\item $(\R, +)$ est un groupe abélien
		\item $(\R^*, \cdot\ )$ est un groupe abélien
		\item $(\R, \cdot\ )$ n'est pas un groupe (pas d'inverse pour $0$)
		\item $\forall n\in \N^*,\ (\M_n(\R), \cdot\ )$ est un groupe non-abélien, où $\M_n(\R)$ est l'ensemble des matrices carrées $n\times n$ et $\cdot$ dénote la multiplication matricielle
	\end{itemize}
\end{example}

\pagebreak
\begin{proposition}[Unicité de l'élément neutre]
	Soit $(G, \cdot\ )$ un groupe et $e \in (G, \cdot\ )$ un élément neutre. L'élément $e$ est l'unique élément neutre de ce groupe.
\end{proposition}

\begin{proof}
	Soit $e,e'$ deux éléments neutre de $(G, \cdot\ )$ satisfaisant (2). Montrons que $e=e'$
	$$\text{(2)}\ \ \forall g \in G,\ [e\cdot g = g\cdot e = g\ \text{et}\ e'\cdot g = g\cdot e' = g]$$
	On en déduit:
	$$e \overset{(2)}= e\cdot e' \overset{(2)}= e'$$
	Donc $e = e'$ on a bien l'unicité de l'élément neutre
\end{proof}

\begin{proposition}[Unicité de l'inverse]
	Soit $(G, \cdot\ )$ un groupe et $g \in (G, \cdot\ )$. L'inverse de $g$ noté $g' \in (G, \cdot\ )$ est unique.
\end{proposition}
	
\begin{proof}
	Soit $h,h'$ deux inverses de $g$ satisfaisant (3). Montrons que $h=h'$ (on n'utilise pas la notation $g^{-1}$ pour éviter de rendre la compréhension plus difficile)
	$$\text{(3)}\ \ g\cdot h = h\cdot g = e\ \text{et}\ g\cdot h' = h'\cdot g = e$$
	On en déduit:
	$$h \overset{(2)}= h\cdot e \overset{(3)}= h\cdot (g\cdot h') \overset{(1)}= (h\cdot g)\cdot h' \overset{(3)}= e\cdot h' \overset{(2)}= h'$$
	Donc $h = h'$ on a bien l'unicité de l'inverse
\end{proof}


\section{Anneaux}
\labsec{anneaux}

Comme on l'a vu dans \refexample{groupes}, $\Z$ muni de l'opération $\boldsymbol{\cdot}$ n'est pas un groupe. $\Z$ possède aussi $2$ opérations, $+$ et $\boldsymbol{\cdot}$. On peut donc imaginer une autre structure algébrique décrivant cette combinaison.

\begin{definition}[Anneaux]
	\labdef{anneaux}
	Un anneau est un ensemble $A$ muni de $2$ opérations binaires
	\begin{align*}
		(\text{addition})\ +: A \times A &\to A\\
		a,b &\mapsto a+b\\
		(\text{multiplication})\ \boldsymbol{\cdot}: A \times A &\to A\\
		a,b &\mapsto a\boldsymbol{\cdot} b
	\end{align*}
	Noté $(A, +, \cdot\ )$, satisfaisant les axiomes suivants
	\begin{enumerate}[label=(\arabic*)]
		\item $(A, +)$ est un \hyperref[def:abéliens]{groupe abélien}
		\item L'ssociativité de la multiplication
			$$\forall a,b,c \in A,\ (a\cdot b)\cdot c = a\cdot (b\cdot c)$$
		\item L'existence d'un élément neutre de la multiplication, noté $1$
			$$\exists 1 \in A,\ \forall a \in A,\ 1\cdot a = a\cdot 1 = a$$
		\item La distributivité est vérifiée
			$$\forall a,b,c \in A,\ (a+b)\cdot c = (a\cdot c)+(b\cdot c)$$
	\end{enumerate}
\end{definition}

\begin{definition}[Anneaux commutatifs]
	\labdef{ann-commutatifs}
	Un anneau commutatif est un \hyperref[def:anneaux]{anneau} satisfaisant également l'axiome (5) de la commutativité de la multiplication
	$$\forall a,b \in G,\ a\cdot b = b\cdot a$$
\end{definition}

\begin{example}
	L'exemple principal qui à motivé notre définition est $(\Z, +, \cdot\ )$ qui est un anneau commutatif. Mais on a aussi $(\M_2(\R), +, \cdot\ )$ et plus généralement $\forall n \in \N^*,\ (\M_n(\R), +, \cdot\ )$ qui sont des anneaux non-commutatifs avec $+$ dénotant l'addition matricielle et $\cdot$ la multiplication.
\end{example}

\section{Corps}
\labsec{corps}

La définition d'un corps est motivée par $\Q$ et $\R$ car ils possèdent une autre propriété intéressante qui n'est pas présente chez $\Z$.

\begin{definition}[Corps]
	\labdef{corps}
	Un corps est un ensemble $\K$ muni de $2$ opérations $+$ et $\cdot$, tel que
	\begin{itemize}
		\item $(\K,+)$ est un \hyperref[def:abéliens]{groupe abélien} avec le symbole $0$ notant l'élément neutre
		\item $(\K \setminus \{0\}, \cdot\ )$ est un \hyperref[def:abéliens]{groupe abélien} avec l'élément neutre $1$
	\end{itemize}
	Et tel que la distributivité est vérifiée
	$$\forall a,b,c \in \K,\ (a+b)\cdot c = (a\cdot c)+(b\cdot c)$$
	De manière équivalente, $(\K, +, \cdot\ )$ est un \hyperref[def:ann-commutatifs]{anneau commutatif} avec deux éléments neutres $0,1$ de l'addition et la multiplication respectivement, tel que
	\begin{itemize}
		\item $0 \neq 1$
		\item Il existe un inverse multiplicatif pout tout élément de $\K$ différent de $0$
			$$\forall k \in \K,\ \exists k^{-1} \in \K,\ k\cdot k^{-1} = k^{-1}\cdot k = 1$$
	\end{itemize}
\end{definition}

\begin{example}
	En exemple on a entre autres $(\Q, +, \cdot\ )$ et $(\R, +, \cdot\ )$ comme ce sont ces deux ensembles qui ont motivés notre définition de corps
\end{example}

\begin{remark}
	Tous les \hyperref[def:groupes]{groupes}, \hyperref[def:anneaux]{anneaux} et \hyperref[def:corps]{corps} qui ont été mentionnés plus haut sont infinis. Il existe aussi des \hyperref[def:groupes]{groupes}, \hyperref[def:anneaux]{anneaux} et \hyperref[def:corps]{corps} finis, voir Série 2.
\end{remark}
