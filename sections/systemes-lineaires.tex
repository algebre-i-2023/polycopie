\setchapterstyle{kao}
\setchapterpreamble[u]{\margintoc}
\chapter{Systèmes linéaires et Matrices}
\labch{sys-lin-matrices}

Ce chapitre introduit les systèmes linéaires et les matrices qui seront utilisés dans les futurs chapitres

\section{Systèmes linéaires}
\labsec{sys-lin}

\begin{definition}[Système linéaire]
    Un système linéaire de $m$ équations à $n$ variables à coefficients dans $\K$ est constitué de $m$ équations de la forme
    $$\begin{matrix}
        a_{11}x_1 &+& a_{12}x_2 &+& \dots &+& a_{1n}x_n &=& b_1\\
        a_{21}x_1 &+& a_{22}x_2 &+& \dots &+& a_{2n}x_n &=& b_2\\
        \vdots && \vdots && \ddots && \vdots && \vdots\\
        a_{m1}x_1 &+& a_{m2}x_2 &+& \dots &+& a_{mn}x_n &=& b_m\\
    \end{matrix}$$
    avec les coefficients $a_{11}, \dots, a_{mn} \in \K$ et les coefficients libres $b_1,\dots,b_m \in \K$\\
    Si $b_1 = \dots = b_m = 0_{\K}$ alors on parle de système \textbf{homogène}
\end{definition}

\begin{definition}[Solution d'un système linéaire]
    Un sous-ensemble de $\K^n$ noté $Sol := \{(\alpha_1,\dots,\alpha_n)\ |\ \alpha_i \in \K,\ i=1,\dots,n\}$ est une solution d'un système linéaire à $m$ équations et $n$ inconnues si $\forall s \in Sol$, remplacer $x_i$ par $\alpha_i,\ i=1,\dots,n$ confirme les $m$ égalités (dans $\K$)
\end{definition}

\begin{example} Quelques exemples de systèmes linéaires (dans $\R$)
    \begin{enumerate}[label=(\alph*)]
        \item $n = 2, m = 2$\\
            $\left\{\begin{matrix}x &+& 2y &=& 2\\x &-& y &=& -4\end{matrix}\right.$
            \quad (Ce système n'est pas homogène)\\
            $Sol = \{(-2,2)\}$\\
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=center,
                    width=5cm,
                    height=5cm,
                ]
                    \addplot[color=blue]{-x/2+1};
                    \addplot[color=red]{x+4};
                \end{axis}
            \end{tikzpicture}

        \item $n = 2, m = 2$\\
            $\left\{\begin{matrix}x &+& 2y &=& 2\\x &+& 2y &=& 0\end{matrix}\right.$
            \quad (Ce système n'est pas homogène)\\
            $Sol = \emptyset$\\
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=center,
                    width=5cm,
                    height=5cm,
                ]
                    \addplot[color=blue]{-x/2+1};
                    \addplot[color=red]{-x/2};
                \end{axis}
            \end{tikzpicture}

        \item $n = 2, m = 2$\\
            $\left\{\begin{matrix}x &+& 2y &=& 0\\2x &+& y &=& 0\end{matrix}\right.$
            \quad (Ce système est homogène)\\
            $Sol = \{(0,0)\}$\\
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=center,
                    width=5cm,
                    height=5cm,
                ]
                    \addplot[color=blue]{-x/2};
                    \addplot[color=red]{-2*x};
                \end{axis}
            \end{tikzpicture}

        \item $n = 2, m = 2$\\
            $\left\{\begin{matrix}x &+& 2y &=& 0\\2x &+& 4y &=& 0\end{matrix}\right.$
            \quad (Ce système est homogène)\\
            $Sol = \{(x,y)\in \R^2\ |\ y = \frac{-x}{2}\}$\\
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=center,
                    width=5cm,
                    height=5cm,
                ]
                    \addplot[color=blue,thick]{-x/2};
                    \addplot[color=red,dashed,thick]{-x/2};
                \end{axis}
            \end{tikzpicture}
        
        \item $n = 2, m = 1$\\
            $0\cdot x + 0\cdot y = 0$
            \quad (Ce système est homogène)\\
            $Sol = \R^2$

        \item $n = 2, m = 3$\\
            $\left\{\begin{matrix}x &+& 2y &=& 0\\2x &+& y &=& 0\\x &-& y &=& 5\end{matrix}\right.$
            \quad (Ce système n'est pas homogène)\\
            $Sol = \emptyset$\\
            \begin{tikzpicture}
                \begin{axis}[
                    axis lines=center,
                    width=5cm,
                    height=5cm,
                ]
                    \addplot[color=blue]{-x/2};
                    \addplot[color=red]{-2*x};
                    \addplot[color=green]{x-5};
                \end{axis}
            \end{tikzpicture}
    \end{enumerate}
\end{example}

\begin{proposition}
    Pour tout système d'équations linéaire homogène on a
    \begin{enumerate}[label=(\arabic*)]
        \item $\alpha_1 = \alpha_2 = \dots = \alpha_n = 0$ est toujours une solution du système
        \item Si $(\alpha_1,\dots,\alpha_n)$ et $(\alpha_1',\dots,\alpha_n')$ sont 2 solutions distinctes, alors $(\alpha_1+\alpha_1',\dots,\alpha_n+\alpha_n')$ est aussi une solution
        \item Si $(\alpha_1,\dots,\alpha_n)$ est une solution, alors $\forall \lambda\in \K,\ (\lambda\cdot\alpha_1,\dots,\lambda\cdot\alpha_n)$ est aussi une solution
    \end{enumerate}
\end{proposition}

\begin{proof}
    Montrons les 3 propositions
    \begin{enumerate}[label=(\arabic*)]
        \item Posons $\alpha_1 = \dots = \alpha_n = 0_{\K}$ alors $\forall i=1,\dots,m,\ a_{i1}\alpha_1 + a_{i2}\alpha_2 + \dots + a_{in}\alpha_n = 0_{\K} = b_i$ comme on est dans un système homogène.\\
            Donc $\alpha_1 = \dots = \alpha_n = 0_{\K}$ est bien une solution du système.
        \item Soient $(\alpha_1,\dots,\alpha_n)$ et $(\alpha_1',\dots,\alpha_n')$ deux solutions du système.\\
            Alors $\forall i = 1,\dots,m$ On a
            \begin{align*}
                &a_{i1}\alpha_1 + \dots + a_{in}\alpha_n = b_i = 0_{\K} = b_i = a_{i1}\alpha_1' + \dots + a_{in}\alpha_n'\\
                &\implies - a_{i1}\alpha_1' - \dots - a_{in}\alpha_n' = 0_{\K}\\
                &\implies a_{i1}{\alpha_1 + \alpha_1'} + \dots + a_{in}(\alpha_n + \alpha_n') = 0_{\K} = b_i
            \end{align*}
            donc $(\alpha_1 + \alpha_1',\dots,\alpha_n + \alpha_n')$ est une solution du système
        \item Soit $(\alpha_1,\dots,\alpha_n)$ une solutions du système.\\
            Alors $\forall i=1,\dots,m,\ a_{i1}\alpha_1 + a_{i2}\alpha_2 + \dots + a_{in}\alpha_n = 0_{\K} = b_i$ comme on est dans un système homogène.\\
            Donc $\forall i = 1,\dots,m,\ \forall \lambda \in \K$ on a
            \begin{align*}
                &\lambda(a_{i1}\alpha_1 + a_{i2}\alpha_2 + \dots + a_{in}\alpha_n) = 0_{\K} = b_i\\
                &= a_{i1} \lambda \alpha_1 + a_{i2} \lambda \alpha_2 + \dots + a_{in} \lambda \alpha_n = 0_{\K} = b_i
            \end{align*}
            Donc $(\lambda\cdot\alpha_1,\dots,\lambda\cdot\alpha_n)$ est aussi une solution
    \end{enumerate}
\end{proof}

\section{Matrices}
\labsec{matrices}

\begin{definition}[Matrice]
    Pour $m,n \geq 1$, on note $\M_{m,n}(\K)$ l'ensembles des matrices de taille $m\times n$ avec coefficients dans $\K$, c'est à dire des tableaux de $m$ lignes et $n$ colonnes de la forme
    $$\begin{pmatrix}
        a_{11} & \dots & a_{1n}\\
        \vdots & \ddots & \vdots\\
        a_{m1} & \dots & a_{mn}
    \end{pmatrix}$$
    avec $a_{i,j}\in K,\ i = 1,\dots,m$ et $j = 1,\dots,n$\\
    Si $m = n$ alors on parle de matrices carrées et on peut écrire $\M_n(\K)$ au lieu de $\M_{m,n}(\K)$
\end{definition}

\begin{definition}[Addition matricielle]
    On définit l'addition matricielle comme suit
    \begin{align*}
        +: \M_{m,n}(\K)\times\M_{m,n}(\K) &\to \M_{m,n}(\K)\\
        A,B &\mapsto (a_{ij} + b_{ij})_{\substack{i=1,\dots,m\\j=1,\dots,n}}
    \end{align*}
    avec $a_{ij}$ correspondant au coefficient à la $i$-ème ligne et $j$-ème colonne de $A$ et $b_{ij}$ correspondant au coefficient à la $i$-ème ligne et $j$-ème colonne de $B$\\
    On obtient alors le résultat suivant
    $$\begin{pmatrix}
        a_{11} & \dots & a_{1n}\\
        \vdots & \ddots & \vdots\\
        a_{m1} & \dots & a_{mn}
    \end{pmatrix} + \begin{pmatrix}
        b_{11} & \dots & b_{1n}\\
        \vdots & \ddots & \vdots\\
        b_{m1} & \dots & b_{mn}
    \end{pmatrix} = \begin{pmatrix}
        a_{11}+b_{11} & \dots & a_{1n}+b_{1n}\\
        \vdots & \ddots & \vdots\\
        a_{m1}+b_{m1} & \dots & a_{mn}+b_{mn}
    \end{pmatrix}$$
\end{definition}

\begin{definition}[Multiplication par un scalaire]
    On définit la multiplication par un scalaire pour les matrices comme suit
    \begin{align*}
        \boldsymbol{\cdot}: \K\times\M_{m,n}(\K) &\to \M_{m,n}(\K)\\
        \lambda,A &\mapsto (\lambda a_{ij})_{\substack{i=1,\dots,m\\j=1,\dots,n}}
    \end{align*}
    avec $a_{ij}$ correspondant au coefficient à la $i$-ème ligne et $j$-ème colonne de $A$\\
    On obtient alors le résultat suivant
    $$\lambda\cdot \begin{pmatrix}
        a_{11} & \dots & a_{1n}\\
        \vdots & \ddots & \vdots\\
        a_{m1} & \dots & a_{mn}
    \end{pmatrix} = \begin{pmatrix}
        \lambda\cdot a_{11} & \dots & \lambda\cdot a_{1n}\\
        \vdots & \ddots & \vdots\\
        \lambda\cdot a_{m1} & \dots & \lambda\cdot a_{mn}
    \end{pmatrix}$$
\end{definition}

\begin{definition}[Multiplication matricielle]
    On définit la multiplication matricielle comme suit
    \begin{align*}
        \boldsymbol{\cdot}: \M_{m,l}(\K)\times\M_{l,n}(\K) &\to \M_{m,n}(\K)\\
        A,B &\mapsto (\sum_{k=1}^{l}a_{ik}b_{kj})_{\substack{i=1,\dots,m\\j=1,\dots,n}}
    \end{align*}
    avec $a_{ij}$ correspondant au coefficient à la $i$-ème ligne et $j$-ème colonne de $A$ et $b_{jk}$ correspondant au coefficient à la $j$-ème ligne et $k$-ème colonne de $B$\\

    On obtient alors le résultat suivant
    $$\begin{pmatrix}
        a_{11} & \dots & a_{1l}\\
        \vdots & \ddots & \vdots\\
        a_{m1} & \dots & a_{ml}
    \end{pmatrix} \cdot \begin{pmatrix}
        b_{11} & \dots & b_{1n}\\
        \vdots & \ddots & \vdots\\
        b_{l1} & \dots & b_{ln}
    \end{pmatrix} =$$$$\begin{pmatrix}
        a_{11}b_{11} + a_{12}b_{21} + \dots + a_{1l}b_{l1} & \dots & a_{11}b_{1n} + a_{12}b_{2n} + \dots + a_{1l}b_{ln}\\
        \vdots & \ddots & \vdots\\
        a_{m1}b_{11} + a_{m2}b_{21} + \dots + a_{ml}b_{l1} & \dots & a_{m1}b_{1n} + a_{m2}b_{2n} + \dots + a_{ml}b_{ln}\\
    \end{pmatrix}$$
\end{definition}

\begin{remark}
    On remarque facilement que l'addition matricielle est associative et commutative et possède l'élément neutre de taille $m \times n$ suivant
    $$\begin{pmatrix}
        0 & \dots & 0\\
        \vdots & \ddots & \vdots\\
        0 & \dots & 0
    \end{pmatrix}$$
    On remarque aussi que la multiplication matricielle est associative et possède l'élément neutre $I_n$ de taille $n\times n$ défini comme suit
    $$\begin{pmatrix}
        1 & 0 & 0 & \dots & 0\\
        0 & 1 & 0 & \dots & 0\\
        0 & 0 & 1 & \dots & 0\\
        \vdots & \vdots & \vdots & \ddots & \vdots\\
        0 & 0 & 0 & \dots & 1
    \end{pmatrix}$$

    On en déduit donc que $(\M_n(\R),+,\boldsymbol{\cdot}\ )$ est un anneau (non-commutatif) avec les opérations d'addition et de multiplication matricielles définies plus haut
\end{remark}

\begin{TODO}
    TODO Example
\end{TODO}